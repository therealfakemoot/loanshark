from setuptools import setup, find_packages

setup(
    name='loanshark',
    description='Asset management and tracking with Loanshark',
    author='Nick Dumas',
    author_email='drakonik@gmail.com',
    url='',
    packages=find_packages(),
    zip_safe=False,
    install_requires=['psycopg2', 'sqlalchemy'],
    entry_points={
        'console_scripts': ['loanshark = loanshark.application:main']
    }
)
