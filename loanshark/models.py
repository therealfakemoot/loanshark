from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Owner(Base):
    '''An Owner is someone or something that owns an Asset.'''
    __tablename__ = 'owner'

    id = Column('id', Integer, primary_key=True)
    name = Column('name', String)
    assets = relationship("Asset", backref='owner')

    def add_asset(self, asset):
        pass

    def add_asset_from_dict(self, asset):
        pass

    def __repr__(self):
        return '<Owner(name={name}, assets={asset_count}>'.format(name=self.name, asset_count=len(self.assets))


class Asset(Base):
    '''An Asset is object which can be owned and lent/borrowed.'''
    __tablename__ = 'asset'

    id = Column('id', Integer, primary_key=True)
    name = Column('name', String)

    owner_id = Column('owner_id', Integer, ForeignKey('owner.id'))
    package_id = Column('package_id', Integer, ForeignKey('package.id'))

    data = Column(JSONB, index=True)

    def __repr__(self):
        return '<Asset(name={name}, owner={owner}>'.format(name=self.name, owner=self.owner.name)


class Loan(Base):
    __tablename__ = 'loan'
    id = Column(Integer, primary_key=True)
    asset_id = Column('asset_id', Integer, ForeignKey('asset.id'))
    holder_id = Column('holder_id', Integer, ForeignKey('owner.id'))
    loan_date = Column('loan_date', DateTime)
    package_id = Column('package_id', Integer, ForeignKey('package.pid'))


class Package(Base):
    __tablename__ = 'package'
    id = Column(Integer, primary_key=True)
    to = Column(Integer, ForeignKey('owner.id'))
    _from = Column('from', Integer, ForeignKey('owner.id'))
    assets = relationship("Asset")
