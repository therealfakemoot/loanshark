import argparse
from contextlib import contextmanager

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from loanshark.models import Owner, Asset


def build_engine(**kwargs):
    conn_string = 'postgresql+psycopg2://{u}:{pass}@localhost:5432/{db}'
    return create_engine(conn_string.format(**kwargs), echo=kwargs.get('echo', False))


def build_database(engine):
    from loanshark.models import Base
    Base.metadata.create_all(engine)


def db_session(engine):
    return sessionmaker(bind=engine)


@contextmanager
def rollback(session):
    """Provide a transactional scope around a series of operations."""
    try:
        yield session
    finally:
        session.rollback()


def cli_main():
    parser = argparse.ArgumentParser(prog='Loanshark v.1', description='Asset management and tracking.')
    parser.add_argument('--add-user', action='store_true')
    parser.add_argument('--add-asset', action='store_true')

    args = parser.parse_args()

    if args.add_user:
        pass
    elif args.add_asset:
        pass


def test():
    session = db_session()
    nick = Owner(name='Nick Dumas')
    session.add(nick)
    session.commit()
    book1 = Asset(name='Dune', owner_id=nick.id)
    book2 = Asset(name='Dungeons and Dragons Handbook', owner_id=nick.id)

    session.add_all([book1, book2])
    session.commit()

    return session.query(Owner).filter(Owner.name == 'Nick Dumas').first()
