from twisted.trial import unittest

from loanshark.models import Owner, Asset


class TestOwner(unittest.TestCase):
    def test_add_asset(self):
        o = Owner(name='Test user')
        a = Asset(name='Test Asset 1', owner_id=o.id)
        b = Asset(name='Test Asset 2', owner_id=o.id)
        o.add_asset(a)
        o.add_asset(b)

        self.assertIn(o.assets, a)

    def test_add_asset_from_dict(self):
        e = Owner('Test user')
        asset_dict = {'name': 'Test asset', 'owner_id': e.id}
        e.add_asset_from_dict(asset_dict)

        # a =
        self.assertIn(e.assets, a)
