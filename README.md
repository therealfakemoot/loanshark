Loanshark is a tool, backed by Peewee ORM, for tracking ownership and 
possession of assets, including but not limited to books, CDs, video
games, and money.
